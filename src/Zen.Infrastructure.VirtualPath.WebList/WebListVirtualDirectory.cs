﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using HtmlAgilityPack;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.WebList
{
    public class WebListVirtualDirectory : AbstractVirtualDirectoryBase
    {
        private readonly HtmlNode _node;
        private readonly WebListVirtualPathProvider _provider;

        public WebListVirtualDirectory(HtmlNode node, WebListVirtualPathProvider provider, IVirtualDirectory parentDirectory)
            : base(provider, parentDirectory)
        {
            _node = node;
            _provider = provider;
        }

        public override DateTime LastModified
        {
            get { return DateTime.Now; }
        }

        public override string RealPath
        {
            get { return _node.XPath; }
        }

        public override IEnumerable<IVirtualFile> Files
        {
            get 
            {
                foreach (var child in _node.ChildNodes)
                {
                    yield return new WebListVirtualFile(child, _provider, this);
                } 
            }
        }

        public override IEnumerable<IVirtualDirectory> Directories
        {
            get { return this.OfType<IVirtualDirectory>(); }
        }

        public override string Name
        {
            get { return _node.Name; }
        }

        public override IEnumerator<IVirtualNode> GetEnumerator()
        {
            return Directories
                .GetEnumerator();
        }

        protected override IVirtualFile GetFileFromBackingDirectoryOrDefault(string fileName)
        {
            return Files.FirstOrDefault(f => f.Name == fileName);
        }

        protected override IVirtualDirectory GetDirectoryFromBackingDirectoryOrDefault(string directoryName)
        {
            return Directories.FirstOrDefault(d => d.Name == directoryName);
        }

        protected override IVirtualFile AddFileToBackingDirectoryOrDefault(string fileName, byte[] contents)
        {
            throw new NotImplementedException();
        }

        protected override void DeleteBackingDirectoryOrFile(string pathToken)
        {
            throw new NotImplementedException();
        }

        protected override IVirtualDirectory AddDirectoryToBackingDirectoryOrDefault(string name)
        {
            var virtualPath = _provider.CombineVirtualPath(VirtualPath, name);
            var dir = GetDirectory(virtualPath);
            if (dir != null)
            {
                return dir;
            }

            return new WebListVirtualDirectory(_node, _provider, this);
        }

        protected override Stream AddFileToBackingDirectoryOrDefault(string fileName)
        {
            throw new NotImplementedException();
        }
    }
}