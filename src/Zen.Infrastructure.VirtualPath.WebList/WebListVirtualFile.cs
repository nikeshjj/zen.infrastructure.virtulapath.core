﻿using System;
using System.IO;
using HtmlAgilityPack;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.WebList
{
    public class WebListVirtualFile : AbstractVirtualFileBase
    {
        private readonly HtmlNode _node;
        private readonly WebListVirtualPathProvider _provider;
        private HtmlNode _fileNode;

        public WebListVirtualFile(HtmlNode node
            , WebListVirtualPathProvider provider
            , WebListVirtualDirectory directory) : base(provider, directory)
        {
            _node = node;
            _provider = provider;
        }

        private HtmlNode FileNode
        {
            get {
                return _fileNode ?? (_fileNode = _node
                    .SelectSingleNode(_provider.CombineVirtualPath(_node.XPath, _provider.FileNameXPath)));
            }
        }

        public override string Name
        {
            get { return FileNode.InnerText; }
        }

        public override string RealPath
        {
            get { return _provider.GetRealPath(_provider.CombineVirtualPath(_node.XPath, _provider.DownloadLinkXPath)); }
        }

        public override DateTime LastModified
        {
            get { return DateTime.Now; }
        }

        public override Stream OpenWrite(WriteMode mode)
        {
            throw new NotImplementedException();
        }

        public override void RenameTo(string newFileName)
        {
            throw new NotImplementedException();
        }

        public override Stream OpenRead()
        {
            return _provider.GetStream(_provider.CombineVirtualPath(_node.XPath, _provider.DownloadLinkXPath));
        }

        protected override IVirtualFile CopyBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            throw new NotImplementedException();
        }

        protected override IVirtualFile MoveBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            throw new NotImplementedException();
        }
    }
}