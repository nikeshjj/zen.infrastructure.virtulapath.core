﻿using System;
using System.IO;
using System.Net;
using HtmlAgilityPack;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.WebList
{
    public class WebListVirtualPathProvider : AbstractVirtualPathProviderBase
    {
        private readonly string url;
        internal readonly string FileNameXPath;
        internal readonly string DownloadLinkXPath;

        public WebListVirtualPathProvider(string url, string fileNameXPath, string downloadLinkXPath)
        {
            this.url = url;
            FileNameXPath = fileNameXPath;
            DownloadLinkXPath = downloadLinkXPath;
        }
        
        private HtmlNode rootNode;
        public HtmlNode RootNode
        {
            get
            {
                if (rootNode == null)
                {
                    var document = new HtmlDocument();
                    using (var client = new WebClient())
                    using (var stream = client.OpenRead(url))
                    {
                        document.Load(stream);
                    }
                    rootNode = document.DocumentNode;
                }
                return rootNode;
            }
        }

        public override IVirtualDirectory GetDirectory(string virtualPath)
        {
            var node = RootNode.SelectSingleNode(virtualPath);
            return new WebListVirtualDirectory(node, this, RootDirectory);
        }

        private IVirtualDirectory rootDirectory;
        public override IVirtualDirectory RootDirectory
        {
            get { return rootDirectory = (rootDirectory ?? new WebListVirtualDirectory(RootNode, this, null)); }
        }

        public override string VirtualPathSeparator
        {
            get { return "/"; }
        }

        public override string RealPathSeparator
        {
            get { return "/"; }
        }

        internal string GetRealPath(string virtualPath)
        {
            var node = RootNode.SelectSingleNode(virtualPath);
            var filePath = node.Attributes["href"].Value;

            if (filePath.StartsWith("/"))
            {
                var downloadRoot = new Uri(url);
                var root = downloadRoot.AbsoluteUri.Replace(downloadRoot.PathAndQuery, "");
                filePath = root + filePath;
            }
            return filePath;
        }

        public Stream GetStream(string virtualPath)
        {
            var node = RootNode.SelectSingleNode(virtualPath);
            var filePath = node.Attributes["href"].Value;

            if (filePath.StartsWith("/"))
            {
                var downloadRoot = new Uri(url);
                var root = downloadRoot.AbsoluteUri.Replace(downloadRoot.PathAndQuery, "");
                filePath = root + filePath;
            }

            using (var client = new WebClient())
            {
                return client.OpenRead(filePath);
            }
        }
    }
}