using System.Composition;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.WebList
{
    [Export("WebList", typeof(IVirtualPathFactory))]
    [Export("ScreenScrape", typeof(IVirtualPathFactory))]
    public class WebListVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            var builder = new ConnectionStringBuilder(connectionString);
            var host = builder.GetString("url", "host");
            var fileNameXPath = builder.GetString("fileNameXPath", "fileName");
            var downloadListXPath = builder.GetString("downloadLinkXPath", "downloadLink");

            return new WebListVirtualPathProvider(host, fileNameXPath, downloadListXPath);
        }
    }
}