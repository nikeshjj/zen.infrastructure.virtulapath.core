﻿using AlexPilotti.FTPS.Client;
using System.ComponentModel.Composition;


namespace Zen.Infrastructure.VirtualPath.ZnFTPS
{
    [Export("ImplicitFTPS", typeof(IVirtualPathProvider))]
    public class FtpsImplicitVirtualPathProvider : FtpsVirtualPathProvider
    {
        public FtpsImplicitVirtualPathProvider(string host, int port, string username, string password)
            : base(ESSLSupportMode.Implicit, host, port, username, password)
        {

        }

        public FtpsImplicitVirtualPathProvider(string host, string username, string password)
            : this(host, 990, username, password)
        {

        }
    }
}
