﻿using System;
using System.ComponentModel.Composition;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.ZnFTPS
{
    [Export("ExplicitFTPS", typeof(IVirtualPathFactory))]
    [Export("FTPS", typeof(IVirtualPathFactory))]
    public class FtpsVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            var builder = new ConnectionStringBuilder(connectionString);
            var host = builder.GetString("host", "server");
            var port = builder.GetString("port", "key");
            var username = builder.GetString("username", "username");
            var password = builder.GetString("password", "password");
            return string.IsNullOrEmpty(port) ? new FtpsVirtualPathProvider(host, username, password) : new FtpsVirtualPathProvider(host, string.IsNullOrEmpty(port) ? default(int) : Convert.ToInt32(port), username, password);
        }
    }
}

