﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.ZnFTPS
{
    internal class FtpVirtualFile : IVirtualFile
    {
        private readonly FtpVirtualPathProvider provider;
        private string path;
        private DateTime lastModified;

        public FtpVirtualFile(FtpVirtualPathProvider provider, string path)
        {
            this.provider = provider;
            this.path = path;
            this.lastModified = DateTime.MinValue;
        }

        public string Extension
        {
            get { return Path.GetExtension(this.path); }
        }

        public string GetFileHash()
        {
            using (var stream = OpenRead())
            {
                return StreamExtensions.ToMd5Hash(stream);
            }
        }

        public System.IO.Stream OpenRead()
        {
            return this.provider.OpenRead(this.path);
        }

        public System.IO.StreamReader OpenText()
        {
            return OpenText(Encoding.UTF8);
        }

        public System.IO.StreamReader OpenText(Encoding encoding)
        {
            return new StreamReader(OpenRead(), encoding);
        }

        public string ReadAllText()
        {
            return ReadAllText(Encoding.UTF8);
        }

        public string ReadAllText(Encoding encoding)
        {
            using (var reader = OpenText(encoding))
            {
                var text = reader.ReadToEnd();
                return text;
            }
        }

        public System.IO.Stream OpenWrite()
        {
            return OpenWrite(WriteMode.Overwrite);
        }

        public System.IO.Stream OpenWrite(WriteMode mode)
        {
            return this.provider.OpenWrite(this.path, mode);
        }

        public IVirtualFile CopyTo(IVirtualDirectory destination)
        {
            return CopyTo(destination, this.Name);
        }

        public IVirtualFile CopyTo(IVirtualDirectory destination, string destFilename)
        {
            return destination.CopyFile(this, destFilename);
        }

        public IVirtualFile CopyTo(string destDirVirtualPath)
        {
            return CopyTo(destDirVirtualPath, this.Name);
        }

        public IVirtualFile CopyTo(string destDirVirtualPath, string destFilename)
        {
            var destination = provider.GetDirectory(destDirVirtualPath);
            return CopyTo(destination, destFilename);
        }

        public IVirtualFile MoveTo(IVirtualDirectory destination)
        {
            return MoveTo(destination, this.Name);
        }

        public IVirtualFile MoveTo(IVirtualDirectory destination, string destFilename)
        {
            return this.provider.Move(this, destination, destFilename);
        }

        public IVirtualFile MoveTo(string destDirVirtualPath)
        {
            return MoveTo(destDirVirtualPath, this.Name);
        }

        public IVirtualFile MoveTo(string destDirVirtualPath, string destFilaname)
        {
            var destination = provider.GetDirectory(destDirVirtualPath);
            return MoveTo(destination, destFilaname);
        }

        public IVirtualPathProvider VirtualPathProvider
        {
            get { return this.provider; }
        }

        public IVirtualDirectory Directory
        {
            get { return this.provider.GetDirectory(provider.GetParentPath(this.path)); }
        }

        public string Name
        {
            get { return Path.GetFileName(this.path); }
        }

        public string VirtualPath
        {
            get { return this.path; }
        }

        public string RealPath
        {
            get { return this.path; }
        }

        public bool IsDirectory
        {
            get { return false; }
        }

        public DateTime LastModified
        {
            get { return this.lastModified; }
        }

        public void Delete()
        {
            this.provider.DeleteFile(this.path);
        }

        public void RenameTo(string newFileName)
        {
            throw new NotImplementedException();
        }
    }
}
