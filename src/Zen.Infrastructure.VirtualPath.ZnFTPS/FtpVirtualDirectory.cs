﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zen.Infrastructure.VirtualPath.ZnFTPS
{
    internal class FtpVirtualDirectory : IVirtualDirectory
    {
        private readonly FtpVirtualPathProvider provider;
        private readonly string path;
        private DateTime lastModified;

        public FtpVirtualDirectory(FtpVirtualPathProvider provider, string path)
        {
            this.provider = provider;
            this.path = path;
            this.lastModified = DateTime.MinValue;
        }

        public bool IsRoot
        {
            get { return this.path == "/"; }
        }

        public IVirtualDirectory ParentDirectory
        {
            get
            {
                if (IsRoot) return null;
                return provider.GetDirectory(provider.GetParentPath(this.path));
            }
        }

        public IEnumerable<IVirtualFile> Files
        {
            get { return this.OfType<IVirtualFile>(); }
        }

        public IEnumerable<IVirtualDirectory> Directories
        {
            get { return this.OfType<IVirtualDirectory>(); }
        }

        public IVirtualFile GetFile(string virtualPath)
        {
            return provider.GetFile(provider.CombineVirtualPath(this.path, virtualPath));
        }

        public IVirtualDirectory GetDirectory(string virtualPath)
        {
            return provider.GetDirectory(provider.CombineVirtualPath(this.path, virtualPath));
        }

        public IEnumerable<IVirtualFile> GetAllMatchingFiles(string globPattern, int maxDepth = 1)
        {
            return this.provider.GetAllMatchingFiles(this.path, globPattern, maxDepth);
        }

        public System.IO.Stream CreateFile(string virtualPath)
        {
            return this.provider.CreateFile(provider.CombineVirtualPath(this.path, virtualPath));
        }

        public IVirtualFile CreateFile(string virtualPath, byte[] contents)
        {
            return this.provider.CreateFile(provider.CombineVirtualPath(this.path, virtualPath), contents);
        }

        public IVirtualFile CreateFile(string virtualPath, string contents)
        {
            return this.provider.CreateFile(provider.CombineVirtualPath(this.path, virtualPath), contents);
        }

        public IVirtualDirectory CreateDirectory(string virtualPath)
        {
            return provider.CreateDirectory(provider.CombineVirtualPath(this.path, virtualPath));
        }

        public void Delete(string virtualPath)
        {
            provider.DeleteDirectory(virtualPath);
        }

        public IVirtualPathProvider VirtualPathProvider
        {
            get { return provider; }
        }

        public IVirtualDirectory Directory
        {
            get { return this; }
        }

        public string Name
        {
            get { return Path.GetFileName(this.path); }
        }

        public string VirtualPath
        {
            get { return path; }
        }

        public string RealPath
        {
            get { return path; }
        }

        public bool IsDirectory
        {
            get { return true; }
        }

        public DateTime LastModified
        {
            get { return this.lastModified; }
        }

        public void Delete()
        {
            provider.DeleteDirectory(this.path);
        }

        public IEnumerator<IVirtualNode> GetEnumerator()
        {
            return provider.GetChildren(this.path);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
