﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.ZnFTPS
{
    [Export("ZnFTPS", typeof(IVirtualPathFactory))]
    [Export("FTP", typeof(IVirtualPathFactory))]
    public class FtpVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            var builder = new ConnectionStringBuilder(connectionString);
            var host = builder.GetString("host", "server");
            var port = builder.GetString("port", "key");
            var username = builder.GetString("username", "username");
            var password = builder.GetString("password", "password");
            return string.IsNullOrEmpty(port) ? new FtpVirtualPathProvider(host, username, password) : new FtpVirtualPathProvider(host, string.IsNullOrEmpty(port) ? default(int) : Convert.ToInt32(port), username, password);
        }
    }
}
