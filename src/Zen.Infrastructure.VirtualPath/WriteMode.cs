﻿namespace Zen.Infrastructure.VirtualPath
{
    public enum WriteMode
    {
        Overwrite,
        Append,
        Truncate
    }
}
