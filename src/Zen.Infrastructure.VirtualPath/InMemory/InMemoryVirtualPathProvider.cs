﻿using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.InMemory
{
    public class InMemoryVirtualPathProvider : AbstractVirtualPathProviderBase
    {
        private readonly InMemoryVirtualDirectory _rootDirectory;

        public InMemoryVirtualPathProvider()
        {
            _rootDirectory = new InMemoryVirtualDirectory(this);
        }
        
        public override IVirtualDirectory RootDirectory
        {
            get { return _rootDirectory; }
        }

        public override string VirtualPathSeparator
        {
            get { return "/"; }
        }

        public override string RealPathSeparator
        {
            get { return "/"; }
        }

        public override IVirtualFile GetFile(string virtualPath)
        {
            return _rootDirectory.GetFile(virtualPath)
                ?? base.GetFile(virtualPath);
        }
    }
}