﻿using System;
using System.IO;

namespace Zen.Infrastructure.VirtualPath.InMemory
{
    public class InMemoryStream : MemoryStream
    {
        private readonly Action<byte[]> _onClose;
        public InMemoryStream(Action<byte[]> onClose)
        {
            _onClose = onClose;
        }

        public InMemoryStream(Action<byte[]> onClose, byte[] data)
            : this(onClose)
        {
            Write(data, 0, data.Length);
            Seek(0, SeekOrigin.Begin);
        }

        public override void Close()
        {
            _onClose(ToArray());
            base.Close();
        }
    }
}