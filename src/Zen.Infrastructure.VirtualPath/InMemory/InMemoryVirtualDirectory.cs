﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.InMemory
{
    public class InMemoryVirtualDirectory : AbstractVirtualDirectoryBase
    {
        private readonly List<InMemoryVirtualFile> _files;
        private readonly List<InMemoryVirtualDirectory> _dirs;
        private readonly DateTime _dirLastModified;

        public InMemoryVirtualDirectory(IVirtualPathProvider owningProvider)
            : this(owningProvider, null) { }

        public InMemoryVirtualDirectory(IVirtualPathProvider owningProvider, IVirtualDirectory parentDirectory)
            : base(owningProvider, parentDirectory)
        {
            _files = new List<InMemoryVirtualFile>();
            _dirs = new List<InMemoryVirtualDirectory>();
            _dirLastModified = DateTime.MinValue;
        }

        public override DateTime LastModified
        {
            get { return _dirLastModified; }
        }



        public override IEnumerable<IVirtualFile> Files
        {
            get { return _files; }
        }

        public override IEnumerable<IVirtualDirectory> Directories
        {
            get { return _dirs; }
        }

        public string DirName { get; set; }

        public override string Name
        {
            get { return DirName; }
        }

        public override IEnumerator<IVirtualNode> GetEnumerator()
        {
            return Files.Cast<IVirtualNode>()
                .Union(Directories)
                .GetEnumerator();
        }

        protected override IVirtualFile GetFileFromBackingDirectoryOrDefault(string fileName)
        {
            return _files.FirstOrDefault(x => x.Name == fileName);
        }

        protected override IEnumerable<IVirtualFile> GetMatchingFilesInDir(string globPattern)
        {
            var matchingFilesInBackingDir = EnumerateFiles(globPattern);
            return matchingFilesInBackingDir;
        }

        protected override IVirtualDirectory GetDirectoryFromBackingDirectoryOrDefault(string directoryName)
        {
            return _dirs.SingleOrDefault(d => d.Name == directoryName);
        }

        protected override IVirtualFile AddFileToBackingDirectoryOrDefault(string fileName, byte[] contents)
        {
            if (Files.Any(f => f.Name == fileName))
            {
                throw new ArgumentException("A file with the same name already exists.");
            }

            var newFile = new InMemoryVirtualFile(VirtualPathProvider, this)
            {
                FilePath = VirtualPathProvider.CombineVirtualPath(VirtualPath, fileName),
                FileName = fileName,
                ByteContents = contents
            };
            _files.Add(newFile);
            return newFile;
        }

        protected override void DeleteBackingDirectoryOrFile(string pathToken)
        {
            var dir = _dirs.FirstOrDefault(d => d.Name == pathToken);
            if (dir != null)
            {
                _dirs.Remove(dir);
            }

            var file = _files.FirstOrDefault(d => d.Name == pathToken);
            if (file != null)
            {
                _files.Remove(file);
            }
        }

        protected override IVirtualDirectory AddDirectoryToBackingDirectoryOrDefault(string name)
        {
            var newDir = new InMemoryVirtualDirectory(VirtualPathProvider, this)
            {
                DirName = name,
            };
            _dirs.Add(newDir);
            return newDir;
        }

        protected override Stream AddFileToBackingDirectoryOrDefault(string fileName)
        {
            if (Files.Any(f => f.Name == fileName))
            {
                throw new ArgumentException("A file with the same name already exists.");
            }

            var newFile = new InMemoryVirtualFile(VirtualPathProvider, this)
            {
                FilePath = VirtualPathProvider.CombineVirtualPath(VirtualPath, fileName),
                FileName = fileName,
                ByteContents = new byte[0],
            };
            var stream = new InMemoryStream((contents) => newFile.ByteContents = contents);
            
            _files.Add(newFile);

            return stream;
        }
    }
}