using System.Composition;

namespace Zen.Infrastructure.VirtualPath.InMemory
{
    [Export("Memory", typeof(IVirtualPathFactory))]
    [Export("InMemory", typeof(IVirtualPathFactory))]
    public class InMemoryVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            return new InMemoryVirtualPathProvider();
        }
    }
}