using System;
using System.IO;
using System.Text;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.InMemory
{
    public class InMemoryVirtualFile : AbstractVirtualFileBase
    {
        public InMemoryVirtualFile(IVirtualPathProvider owningProvider, IVirtualDirectory directory)
            : base(owningProvider, directory)
        {
            FileLastModified = DateTime.MinValue;
        }

        public string FilePath { get; set; }

        public string FileName { get; set; }
        public override string Name
        {
            get { return FileName; }
        }

        public DateTime FileLastModified { get; set; }
        public override DateTime LastModified
        {
            get { return FileLastModified; }
        }

        public byte[] ByteContents { get; set; }

        public override Stream OpenRead()
        {
            return new MemoryStream(ByteContents ?? Encoding.UTF8.GetBytes(""));
        }

        public override Stream OpenWrite(WriteMode mode)
        {
            if (mode == WriteMode.Truncate)
            {
                return new InMemoryStream(data =>
                {
                    ByteContents = data;
                });
            }
            else
            {
                var stream = new InMemoryStream(data =>
                {
                    ByteContents = data;
                }, ByteContents ?? new byte[0]);

                if (mode == WriteMode.Append)
                {
                    stream.Seek(stream.Length, SeekOrigin.Begin);
                }

                return stream;
            }
        }

        public override void RenameTo(string newFileName)
        {
            FileName = newFileName;
        }

        protected override IVirtualFile CopyBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            return directory.CopyFile(this, name);
        }

        protected override IVirtualFile MoveBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            var newFile = directory.CopyFile(this, name);
            Delete();
            return newFile;
        }
    }
}