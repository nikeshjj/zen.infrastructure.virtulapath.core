﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zen.Infrastructure.VirtualPath.Common
{
    public class ConnectionStringBuilder
    {
        private readonly string _connectionString;
        private IDictionary<string, string> _pairs;

        public ConnectionStringBuilder(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string GetString(params string[] keyAliases)
        {
            var keyValuePairs = ParseConnectionString();

            foreach (var alias in keyAliases)
            {
                string value;
                if (keyValuePairs.TryGetValue(alias, out value))
                    return value;
            }

            return string.Empty;
        }

        public T GetEnum<T>(params string[] keyAliases) where T : struct
        {
            var stringValue = GetString(keyAliases);

            T result;
            if (Enum.TryParse(stringValue, out result))
                return result;

            throw new Exception("Value is not contained within the Enum");
        }

        public int GetInt32(params string[] keyAliases)
        {
            var stringValue = GetString(keyAliases);

            int result;
            if (int.TryParse(stringValue, out result))
                return result;

            throw new Exception("Value is not an Integer");
        }

        private IDictionary<string, string> ParseConnectionString()
        {
            return _pairs ?? (_pairs = _connectionString.Split(';')
                .Where(kvp => kvp.Contains('='))
                .Select(kvp => kvp.Split(new[] {'='}, 2))
                .ToDictionary(kvp => kvp[0].Trim(),
                    kvp => kvp[1].Trim(),
                    StringComparer.InvariantCultureIgnoreCase));
        }
    }
}
