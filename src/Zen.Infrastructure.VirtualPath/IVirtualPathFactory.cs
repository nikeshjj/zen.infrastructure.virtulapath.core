﻿namespace Zen.Infrastructure.VirtualPath
{
    public interface IVirtualPathFactory
    {
        IVirtualPathProvider CreateProvider(string connectionString);
    }
}