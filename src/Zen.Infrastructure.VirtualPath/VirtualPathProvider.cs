﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Net;

namespace Zen.Infrastructure.VirtualPath
{
    public class VirtualPathProvider
    {
        private static CompositionContainer _container;

        private static CompositionContainer Container
        {
            get
            {
                if (_container == null)
                {
                    var catalog = new AggregateCatalog();
                    catalog.Catalogs.Add(new DirectoryCatalog(GetBaseBinPath(), "Zen.Infrastructure.VirtualPath*dll"));
                    _container = new CompositionContainer(catalog);
                }

                return _container;
            }
        }

        private static string GetBaseBinPath()
        {
            return AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin"));
        }

        public static IVirtualPathProvider Open(string providerName)
        {
            return Open(providerName, string.Empty);
        }

        public static IVirtualPathProvider Open(string providerName, string connectionString)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var factory = Container.GetExportedValueOrDefault<IVirtualPathFactory>(providerName);

            if (factory != null)
                return factory.CreateProvider(connectionString);
            
            throw new NotSupportedException(
                string.Format("No provider was with the providerName: \"{0}\".", providerName));
        }
    }
}
