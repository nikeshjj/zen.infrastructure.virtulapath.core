﻿using System;
using System.Runtime.Serialization;

namespace Zen.Infrastructure.VirtualPath
{
    [Serializable]
    public class VirtualPathException : Exception
    {
        public VirtualPathException() { }
        public VirtualPathException(string message) : base(message) { }
        public VirtualPathException(string message, Exception inner) : base(message, inner) { }
        protected VirtualPathException(
          SerializationInfo info,
          StreamingContext context)
            : base(info, context) { }
    }
}
