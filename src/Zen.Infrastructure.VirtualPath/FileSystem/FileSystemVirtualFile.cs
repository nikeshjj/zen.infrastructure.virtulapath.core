﻿using System;
using System.IO;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.FileSystem
{
    public class FileSystemVirtualFile : AbstractVirtualFileBase
    {
        private FileInfo _backingFile;

        public FileSystemVirtualFile(IVirtualPathProvider owningProvider, IVirtualDirectory directory, FileInfo fInfo)
            : base(owningProvider, directory)
        {
            _backingFile = fInfo;
        }

        public override string Name
        {
            get { return _backingFile.Name; }
        }

        public override string RealPath
        {
            get { return _backingFile.FullName; }
        }

        public override DateTime LastModified
        {
            get { return _backingFile.LastWriteTime; }
        }
        
        public override Stream OpenRead()
        {
            return _backingFile.OpenRead();
        }

        public override void Delete()
        {
            _backingFile.Delete();
        }

        public override Stream OpenWrite(WriteMode mode)
        {
            switch (mode)
            {
                case WriteMode.Overwrite: 
                    return _backingFile.OpenWrite();
                case WriteMode.Append: 
                    return _backingFile.Open(FileMode.Append, FileAccess.Write);
                case WriteMode.Truncate: 
                    return _backingFile.Open(FileMode.Truncate, FileAccess.Write);
                default: 
                    throw new NotImplementedException();
            }
        }

        public override void RenameTo(string newFileName)
        {
            var fullPath = Path.Combine(_backingFile.Directory.FullName, newFileName);
            var newBackingFile = _backingFile.CopyTo(fullPath, true);
            _backingFile.Delete();
            _backingFile = newBackingFile;
        }

        protected override IVirtualFile CopyBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            if (directory is FileSystemVirtualDirectory)
            {
                var copyFInfo = _backingFile.CopyTo(Path.Combine(directory.RealPath, name), true);
                return new FileSystemVirtualFile(VirtualPathProvider, directory, copyFInfo);
            }
            
            return directory.CopyFile(this, name);
        }

        protected override IVirtualFile MoveBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            if (directory is FileSystemVirtualDirectory)
            {
                _backingFile.MoveTo(Path.Combine(directory.RealPath, name));
                return new FileSystemVirtualFile(VirtualPathProvider, directory, _backingFile);
            }
            
            var newFile = directory.CopyFile(this, name);
            Delete();
            return newFile;
        }
    }
}
