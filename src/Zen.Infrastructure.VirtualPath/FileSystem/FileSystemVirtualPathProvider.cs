﻿using System;
using System.IO;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.FileSystem
{
    public class FileSystemVirtualPathProvider : AbstractVirtualPathProviderBase
    {
        private readonly FileSystemVirtualDirectory _rootDir;
        
        public FileSystemVirtualPathProvider(string rootPath)
            : this(new DirectoryInfo(rootPath))
        { }

        public FileSystemVirtualPathProvider(DirectoryInfo rootDirInfo)
        {
            if (rootDirInfo == null)
                throw new ArgumentNullException("rootDirInfo");

            var rootDirInfo1 = rootDirInfo;

            if (!rootDirInfo1.Exists)
                throw new ApplicationException(
                    string.Format("RootDir '{0}' for virtual path does not exist", rootDirInfo1.FullName));

            _rootDir = new FileSystemVirtualDirectory(this, null, rootDirInfo1);
        }

        public override IVirtualDirectory RootDirectory { get { return _rootDir; } }
        public override string VirtualPathSeparator { get { return "/"; } }
        public override string RealPathSeparator { get { return Convert.ToString(Path.DirectorySeparatorChar); } }
    }
}