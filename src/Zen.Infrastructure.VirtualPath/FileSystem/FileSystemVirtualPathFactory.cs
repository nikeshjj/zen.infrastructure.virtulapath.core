﻿
using System.Composition;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.FileSystem
{
    [Export("FileSystem", typeof(IVirtualPathFactory))]
    [Export("FS", typeof(IVirtualPathFactory))]
    public class FileSystemVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            var builder = new ConnectionStringBuilder(connectionString);
            var path = builder.GetString("rootPath", "rootDirInfo", "root", "path");

            if (string.IsNullOrEmpty(path))
                path = connectionString;

            return new FileSystemVirtualPathProvider(path);
        }
    }
}