using System.ComponentModel.Composition;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.SshNet
{
    [Export("SFTP", typeof(IVirtualPathFactory))]
    [Export("SSHFTP", typeof(IVirtualPathFactory))]
    public class SftpVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            var builder = new ConnectionStringBuilder(connectionString);
            var host = builder.GetString("host", "server");
            var port = builder.GetInt32("port");
            var username = builder.GetString("username", "user");
            var password = builder.GetString("password", "secret");

            return new SftpVirtualPathProvider(host, port, username, password);
        }
    }
}