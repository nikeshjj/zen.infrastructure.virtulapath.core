﻿using System;
using System.IO;
using Renci.SshNet.Sftp;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.SshNet
{
    public class SftpVirtualFile : AbstractVirtualFileBase
    {
        private readonly SftpVirtualPathProvider _provider;
        private readonly SftpFile _file;
        private readonly Lazy<SftpFile> _lazyFile;

        public SftpVirtualFile(SftpVirtualPathProvider owningProvider, IVirtualDirectory directory, string name, DateTime? lastModified)
            : base(owningProvider, directory)
        {
            _provider = owningProvider;
            this.name = name;
            this.lastModified = lastModified ?? DateTime.MinValue;
            _lazyFile = new Lazy<SftpFile>(() => _file ?? _provider.GetSftpFile(VirtualPath));
        }

        public SftpVirtualFile(SftpVirtualPathProvider owningProvider, IVirtualDirectory directory, SftpFile file)
            : this(owningProvider, directory, file.Name, file.LastWriteTime)
        {
            _provider = owningProvider;
            _file = file;
        }

        private readonly string name;
        public override string Name
        {
            get { return name; }
        }

        private readonly DateTime lastModified;
        public override DateTime LastModified
        {
            get { return lastModified; }
        }

        public override Stream OpenRead()
        {
            return _provider.GetStream(VirtualPath);
        }

        public override Stream OpenWrite(WriteMode mode)
        {
            return _provider.OpenWrite(VirtualPath, mode);
        }

        public override void RenameTo(string newFileName)
        {
            var oldPath = _provider.CombineVirtualPath(Directory.VirtualPath, Name);
            var newPath = _provider.CombineVirtualPath(Directory.VirtualPath, newFileName);
            _provider.RenameFile(oldPath, newPath);
        }

        protected override IVirtualFile CopyBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            return directory.CopyFile(this, name);
        }

        protected override IVirtualFile MoveBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            if (directory is SftpVirtualDirectory)
            {
                var dir = (SftpVirtualDirectory)directory;
                if (dir.Provider == _provider)
                {
                    ((SftpVirtualDirectory)Directory).RemoveFromCache(this);
                    _lazyFile.Value.MoveTo(_provider.CombineVirtualPath(directory.VirtualPath, name));
                    return new SftpVirtualFile(_provider, dir, name, DateTime.Now);
                }
            }

            var newFile = directory.CopyFile(this, name);
            Delete();
            return newFile;
        }
    }
}
