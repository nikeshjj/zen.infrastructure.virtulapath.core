﻿using System;
using System.Collections.Generic;
using System.IO;
using Zen.Infrastructure.VirtualPath.Common;
using Zen.Infrastructure.VirtualPath.InMemory;
using Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client;

namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp
{
    public class BrickFtpVirtualPathProvider : AbstractVirtualPathProviderBase
    {
        private readonly BrickFtpApiClient _client;

        private BrickFtpApiClient ConnectedClient
        {
            get
            {
                return _client;
            }
        }

        public BrickFtpVirtualPathProvider(BrickFtpApiClient client)
        {
            _client = client;
        }
        
        public BrickFtpVirtualPathProvider(string host, string apiKey)
            : this(GetClient(host,apiKey)) { }

        private static BrickFtpApiClient GetClient(string host,string apiKey)
        {
            return new BrickFtpApiClient(host,apiKey);
        }

        private IVirtualDirectory _rootDirectory;
        public override IVirtualDirectory RootDirectory
        {
            get { return _rootDirectory = (_rootDirectory ?? new BrickFtpVirtualDirectory(this, null)); }
        }

        public override string VirtualPathSeparator
        {
            get { return "/"; }
        }

        public override string RealPathSeparator
        {
            get { return "/"; }
        }

        internal IEnumerable<BrickItem> GetContents(string virtualPath)
        {
            return ConnectedClient.ListDirectory(virtualPath);
        }

        internal void DeleteDirectory(string virtualPath)
        {
            ConnectedClient.Delete(virtualPath);
        }

        internal void CreateDirectoryInternal(string virtualPath)
        {
            ConnectedClient.CreateDirectory(virtualPath);
        }

        internal Stream CreateFileInternal(string virtualPath)
        {
            throw new NotImplementedException();
        }

        internal void CreateFileInternal(string virtualPath, byte[] contents)
        {
            ConnectedClient.WriteAllBytes(virtualPath, contents);
        }

        internal void RenameFile(string oldPath, string newPath)
        {
            ConnectedClient.RenameFile(oldPath, newPath);
        }

        internal void DeleteFile(string virtualPath)
        {
            ConnectedClient.Delete(virtualPath);
        }

        internal Stream GetStream(string virtualPath)
        {
            var stream = new MemoryStream();
            ConnectedClient.DownloadFile(virtualPath, stream);
            stream.Position = 0;
            return stream;
        }

        internal Stream OpenWrite(string virtualPath)
        {
            using(var stream = GetStream(virtualPath))
            {
                var bytes = StreamExtensions.ReadStreamToEnd(stream);
                return new InMemoryStream(
                    (data) => ConnectedClient.WriteAllBytes(virtualPath, data), 
                    bytes);
            }
        }

        internal Stream OpenWrite(string virtualPath, WriteMode mode)
        {
            Action<byte[]> onClose = (data) => ConnectedClient.WriteAllBytes(virtualPath, data);

            if (mode == WriteMode.Truncate)
            {
                ConnectedClient.Delete(virtualPath);
                return new InMemoryStream(onClose);
            }
            else if (mode == WriteMode.Overwrite)
            {
                return new InMemoryStream(onClose);
            }
            else
            {
                var bytes = default(byte[]);
                using(var readStream = GetStream(virtualPath))
                {
                    bytes = StreamExtensions.ReadStreamToEnd(readStream);
                }
                var stream = new InMemoryStream(onClose, bytes);
                stream.Seek(stream.Length, SeekOrigin.Begin);
                return stream;
            }
        }

        internal BrickFile GetBrickFile(string virtualPath)
        {
            return ConnectedClient.GetFile(virtualPath);
        }

        public void MoveTo(string sourcePath, string destinationPath)
        {
            ConnectedClient.RenameFile(sourcePath, destinationPath);
        }
    }
}
