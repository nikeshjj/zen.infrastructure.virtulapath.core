﻿using System;
using System.IO;
using Zen.Infrastructure.VirtualPath.Common;
using Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client;

namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp
{
    public class BrickFtpVirtualFile : AbstractVirtualFileBase
    {
        private readonly BrickFtpVirtualPathProvider _provider;
        private readonly BrickItem _file;
        private readonly Lazy<BrickItem> _lazyFile;

        public BrickFtpVirtualFile(BrickFtpVirtualPathProvider owningProvider, IVirtualDirectory directory, string name, DateTime? lastModified)
            : base(owningProvider, directory)
        {
            _provider = owningProvider;
            _name = name;
            _lastModified = lastModified ?? DateTime.MinValue;
            _lazyFile = new Lazy<BrickItem>(() => _file ?? _provider.GetBrickFile(VirtualPath));
        }

        public BrickFtpVirtualFile(BrickFtpVirtualPathProvider owningProvider, IVirtualDirectory directory, BrickItem file)
            : this(owningProvider, directory, file.Name, file.mtime)
        {
            _provider = owningProvider;
            _file = file;
        }

        private readonly string _name;
        public override string Name
        {
            get { return _name; }
        }

        private readonly DateTime _lastModified;
        public override DateTime LastModified
        {
            get { return _lastModified; }
        }

        public override Stream OpenRead()
        {
            return _provider.GetStream(VirtualPath);
        }

        public override Stream OpenWrite(WriteMode mode)
        {
            return _provider.OpenWrite(VirtualPath, mode);
        }

        public override void RenameTo(string newFileName)
        {
            var oldPath = _provider.CombineVirtualPath(Directory.VirtualPath, Name);
            var newPath = _provider.CombineVirtualPath(Directory.VirtualPath, newFileName);
            _provider.RenameFile(oldPath, newPath);
        }

        protected override IVirtualFile CopyBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            return directory.CopyFile(this, name);
        }

        protected override IVirtualFile MoveBackingFileToDirectory(IVirtualDirectory directory, string name)
        {
            if (directory is BrickFtpVirtualDirectory)
            {
                var dir = (BrickFtpVirtualDirectory)directory;
                if (dir.Provider == _provider)
                {
                    ((BrickFtpVirtualDirectory)Directory).RemoveFromCache(this);
                    _provider.MoveTo(_lazyFile.Value.path, _provider.CombineVirtualPath(directory.VirtualPath, name));
                    return new BrickFtpVirtualFile(_provider, dir, name, DateTime.Now);
                }
            }

            var newFile = directory.CopyFile(this, name);
            Delete();
            return newFile;
        }
    }
}
