﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Zen.Infrastructure.VirtualPath.Common;
using Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client;

namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp
{
    public class BrickFtpVirtualDirectory : AbstractVirtualDirectoryBase
    {
        internal readonly BrickFtpVirtualPathProvider Provider;
        private readonly BrickItem _file;

        public BrickFtpVirtualDirectory(BrickFtpVirtualPathProvider owningProvider, IVirtualDirectory parentDirectory, 
            string name = null, DateTime? lastModified = null) 
            : base(owningProvider, parentDirectory)
        {
            Provider = owningProvider;
            _name = name;
            _lastModified = lastModified ?? DateTime.MinValue;
        }

        private BrickFtpVirtualDirectory(BrickFtpVirtualPathProvider owningProvider, IVirtualDirectory parentDirectory, BrickItem file)
            : this(owningProvider, parentDirectory, file.Name, file.mtime)
        {
            _file = file;
        }

        private readonly DateTime _lastModified;
        public override DateTime LastModified
        {
            get 
            {
                return _lastModified;
            }
        }

        public override IEnumerable<IVirtualFile> Files
        {
            get { return this.OfType<IVirtualFile>(); }
        }

        public override IEnumerable<IVirtualDirectory> Directories
        {
            get { return this.OfType<IVirtualDirectory>(); }
        }

        private List<BrickItem> _contents;
        private IEnumerable<BrickItem> Contents
        {
            get
            {
                return _contents = (_contents ?? Provider.GetContents(VirtualPath).ToList());
            }
        }

        private readonly string _name;
        public override string Name
        {
            get { return _name; }
        }

        public override IEnumerator<IVirtualNode> GetEnumerator()
        {
            return Contents
                .Select(c => c.IsDirectory
                    ? (IVirtualNode)new BrickFtpVirtualDirectory(Provider, this, c)
                    : (IVirtualNode)new BrickFtpVirtualFile(Provider, this, c))
                .GetEnumerator();
        }

        protected override IVirtualFile GetFileFromBackingDirectoryOrDefault(string fileName)
        {
            return Files.FirstOrDefault(f => f.Name == fileName);
        }

        protected override IVirtualDirectory GetDirectoryFromBackingDirectoryOrDefault(string directoryName)
        {
            return Directories.FirstOrDefault(d => d.Name == directoryName);
        }

        protected override void DeleteBackingDirectoryOrFile(string pathToken)
        {
            var node = this.FirstOrDefault(n => n.Name == pathToken);
            if (node == null) return;

            if (node is IVirtualFile)
            {
                var file = (IVirtualFile)node;
                Provider.DeleteFile(file.VirtualPath);
            }
            else if (node is IVirtualDirectory)
            {
                var dir = (IVirtualDirectory)node;
                foreach (var subNode in dir.ToArray())
                {
                    subNode.Delete();
                }
                Provider.DeleteDirectory(dir.VirtualPath);
            }

            RemoveFromCache(node);
        }

        internal void RemoveFromCache(IVirtualNode node)
        {
            if (_contents != null)
            {
                _contents.RemoveAll(c => c.Name == node.Name);
            }
        }

        protected override IVirtualDirectory AddDirectoryToBackingDirectoryOrDefault(string name)
        {
            var virtualPath = Provider.CombineVirtualPath(VirtualPath, name);
            var dir = GetDirectory(virtualPath);
            if (dir != null)
            {
                return dir;
            }

            Provider.CreateDirectoryInternal(virtualPath);

            // TODO: add local collection and make Contents merge both
            _contents = null;

            return new BrickFtpVirtualDirectory(Provider, this, name);
        }

        protected override IVirtualFile AddFileToBackingDirectoryOrDefault(string fileName, byte[] contents)
        {
            if (Files.Any(f => f.Name == fileName))
            {
                throw new ArgumentException("File already exists.");
            }

            var virtualPath = Provider.CombineVirtualPath(VirtualPath, fileName);
            Provider.CreateFileInternal(virtualPath, contents);

            // TODO: add local collection and make Contents merge both
            _contents = null;
            return new BrickFtpVirtualFile(Provider, this, fileName, DateTime.Now);
        }

        protected override Stream AddFileToBackingDirectoryOrDefault(string fileName)
        {
            if (Files.Any(f => f.Name == fileName))
            {
                throw new ArgumentException("File already exists.");
            }

            var virtualPath = Provider.CombineVirtualPath(VirtualPath, fileName);
            _contents = null;
            return Provider.CreateFileInternal(virtualPath);
        }

        public override IVirtualDirectory GetDirectory(Stack<string> virtualPath)
        {
            if (virtualPath.Count == 0)
                return new BrickFtpVirtualDirectory(Provider, null);

            return base.GetDirectory(virtualPath);
        }
    }
}
