﻿namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client
{
    public class BrickUser
    {
        public int id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string notes { get; set; }
    }
}