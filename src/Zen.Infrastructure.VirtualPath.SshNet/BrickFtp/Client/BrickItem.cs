﻿using System;

namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client
{
    public class BrickItem
    {
        public string path { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public DateTime mtime { get; set; }
        public string crc32 { get; set; }
        public string md5 { get; set; }
        public bool IsDirectory{ get { return type == "directory"; }}
        public bool IsFile { get { return type == "file"; } }

        public string Name
        {
            get
            {
                var split = path.Split(new[] {'/'});
                return split[split.Length - 1];
            }
        }
    }
}