﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client
{
    public class BrickFtpApiClient
    {
        private readonly RestClient _client;
        
        public BrickFtpApiClient(string host, string apiKey)
        {
            //"https://znergysuite.brickftp.com/api/rest/v1"
            _client = new RestClient(host)
            {
                Authenticator = new HttpBasicAuthenticator(apiKey, "x")
            };
        }

        public BrickUser[] GetUsers()
        {
            var request = new RestRequest("/users", Method.GET);
            var response = _client.Execute<List<BrickUser>>(request);
            return response.Data.ToArray();
        }

        public BrickItem[] ListDirectory(string path)
        {
            var request = new RestRequest("/folders/{path}?page=1", Method.GET);
            request.AddUrlSegment("path", path);
            var response = _client.Execute<List<BrickItem>>(request);
            return response.Data.ToArray();
        }

        public BrickFile GetFile(string path)
        {
            var request = new RestRequest("/files/{path}", Method.GET);
            request.AddUrlSegment("path", path);
            var response = _client.Execute<BrickFile>(request);
            return response.Data;
        }

        public void DownloadFile(string path, Stream stream)
        {
            if (string.IsNullOrEmpty(path))
                return;
            
            var file = GetFile(path);

            var client = new WebClient();
            var result = client.DownloadData(file.download_uri);
            using (var inMemory = new MemoryStream(result))
            {
                inMemory.CopyTo(stream);
            }
        }

        public void RenameFile(string sourcePath, string destinationPath)
        {
            var request = new RestRequest("/files/{path}", Method.POST);
            request.AddUrlSegment("path", sourcePath);
            request.RequestFormat = DataFormat.Json;
            var body = new Dictionary<string, string>
            {
                {"move-destination", destinationPath}
            };
            request.AddJsonBody(body);
            _client.Execute(request);
        }

        public void Delete(string path)
        {
            var request = new RestRequest("/files/{path}", Method.DELETE);
            request.AddUrlSegment("path", path);
            _client.Execute(request);
        }

        public void CreateDirectory(string path)
        {
            var request = new RestRequest("/folders/{path}", Method.POST);
            request.AddUrlSegment("path", path);
            _client.Execute(request);
        }
        
        public IRestResponse<BrickFile> WriteAllBytes(string path, byte[] data)
        {
            // Showing Intent to Brick FTP
            var restRequest = new RestRequest("/files/{path}", Method.POST);
            restRequest.AddUrlSegment("path", path);
            restRequest.RequestFormat = DataFormat.Json;
            var body = new Dictionary<string, string>
            {
                {"action", "put"},
            };
            restRequest.AddJsonBody(body);
            var restResponse = _client.Execute<BrickFile>(restRequest);
            if (restResponse.StatusCode != HttpStatusCode.OK)
                return restResponse;
            var brickResponse = JsonConvert.DeserializeObject<BrickResp>(restResponse.Content, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            // Uploading To URI from response
            var uploadclient = new RestClient(brickResponse.upload_uri);
            var uploadRequest = new RestRequest {Method = Method.PUT};
            uploadRequest.AddParameter("filecontent", data, ParameterType.RequestBody);
            var uploadresponse = uploadclient.Execute<BrickFile>(uploadRequest);
            if (uploadresponse.StatusCode != HttpStatusCode.OK) 
                return uploadresponse;

            // Complete Step
            var completeRequest = new RestRequest("/files/{path}", Method.POST);
            completeRequest.AddUrlSegment("path", path);
            var body2 = new Dictionary<string, string>
            {
                {"action", "end"},
                {"ref", brickResponse.@ref}
            };
            completeRequest.AddJsonBody(body2);
            var endResponse = _client.Execute<BrickFile>(completeRequest);
            return endResponse;
        }
    }

    public class BrickResp
    {
        public string @ref { get; set; }
        public string upload_uri { get; set; }
    }

}
