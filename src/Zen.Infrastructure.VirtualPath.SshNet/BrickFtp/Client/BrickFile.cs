﻿namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client
{
    public class BrickFile : BrickItem
    {
        public string permissions { get; set; }
        public string download_uri { get; set; }
    }
}