using System.ComponentModel.Composition;
using Zen.Infrastructure.VirtualPath.Common;

namespace Zen.Infrastructure.VirtualPath.SshNet.BrickFtp
{
    [Export("BrickFtp", typeof(IVirtualPathFactory))]
    [Export("Brick", typeof(IVirtualPathFactory))]
    public class BrickFtpVirtualPathFactory : IVirtualPathFactory
    {
        public IVirtualPathProvider CreateProvider(string connectionString)
        {
            var builder = new ConnectionStringBuilder(connectionString);
            var host = builder.GetString("host", "server");
            var apiKey = builder.GetString("apiKey", "key");

            return new BrickFtpVirtualPathProvider(host,apiKey);
        }
    }
}