﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Zen.Infrastructure.VirtualPath.WebList.Tests
{
    
    public class ErcotSampleSpike
    {
        [TestCase]
        public void ErcotSettlement()
        {
            var connectionDictionary = new Dictionary<string, string>
            {
                { "url", "http://mis.ercot.com/misapp/GetReports.do?reportTypeId=12301&reportTitle=Download&showHTMLView=&mimicKey" },
                { "fileNameXPath", "/td[1]"},
                { "downloadLinkXPath", "/td[4]/div[1]/a[1]" }
            };

            var connectionString = string.Join(";",
                connectionDictionary.Select(x => string.Format("{0}={1}", x.Key, x.Value)));

            var provider = VirtualPathProvider.Open("WebList", connectionString);
            
            var directory = provider.GetDirectory("/html[1]/body[1]/table[1]/tr[2]/td[1]/table[1]");

            Execute(directory);
        }

        public void Execute(IVirtualDirectory directory)
        {
            foreach (var file in directory.Files)
            {
                if (file.Extension == ".zip")
                {
                    Console.WriteLine(file.Name);
                }
            }
        }
    }
}
