﻿using System;
using System.IO;
using NUnit.Framework;
using Zen.Infrastructure.VirtualPath.FileSystem;

namespace Zen.Infrastructure.VirtualPath.Tests.FileSystem
{
    public class FileSystemVirtualPathProviderTests : AbstractVirtualPathProviderFixture
    {
        public FileSystemVirtualPathProviderTests()
            : base(GetProvider)
        {

        }

        private const string RootPath = "C:\\Temp\\vpathtests";

        private static IVirtualPathProvider GetProvider()
        {
            if (!Directory.Exists(RootPath))
            {
                Directory.CreateDirectory(RootPath);
            }
            return new FileSystemVirtualPathProvider(RootPath);
        }

        public override void SetUp()
        {
			if (!Directory.Exists(RootPath))
			{
				Directory.CreateDirectory(RootPath);
			}
            base.SetUp();
        }

        [Test]
        public void ShouldThrowExceptionWhenDirectoryIsNotProvided()
        {
            Assert.Throws<ArgumentNullException>(() => new FileSystemVirtualPathProvider(default(DirectoryInfo)));
        }

        [Test]
        public void ShouldThrowExceptionWhenDirectoryDoesNotExists()
        {
            Assert.Throws<ApplicationException>(() => new FileSystemVirtualPathProvider("C:\\NotExisting\\"));
        }
    }
}
