﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Zen.Infrastructure.VirtualPath.InMemory;

namespace Zen.Infrastructure.VirtualPath.Tests
{
    public abstract class AbstractVirtualPathProviderFixture
    {
        private readonly Func<IVirtualPathProvider> _providerFac;
        private IVirtualPathProvider _provider;

        protected AbstractVirtualPathProviderFixture(Func<IVirtualPathProvider> providerFac)
        {
            _providerFac = providerFac;
        }

        [SetUp]
        public virtual void SetUp()
        {
            _provider = _providerFac();
        }

        [TearDown]
        public virtual void TearDown()
        {
            if (_provider != null)
            {
                Clear();
                _provider.Dispose();
            }
        }

        private void Clear()
        {
            foreach (var node in _provider.RootDirectory.ToArray())
            {
                node.Delete();
            }
        }

        [Test]
        public void AddFile_CreateFile()
        {
            var name = "test.txt";
            var file = _provider.CreateFile(name, "");

            Assert.That(file, Is.Not.Null);
            Assert.That(file.IsDirectory, Is.False);
            Assert.That(file.Name, Is.EqualTo(name));
        }

        [Test]
        public void AddFile_AddFileToProvider()
        {
            var name = "Test.txt";
            var file = _provider.CreateFile(name, "");

            Assert.That(_provider.GetFile(name), Is.EqualTo(file));
        }

        [Test]
        public void AddFile_AddFileToRootDirectory()
        {
            var name = "test.txt";
            var file = _provider.CreateFile(name, "");

            Assert.That(_provider.RootDirectory.GetFile(name), Is.EqualTo(file));
        }

        [Test]
        public void AddFile_CreateFileWithStringContent()
        {
            var content = "blabla";
            var file = _provider.CreateFile("test.txt", content);
            Assert.That(file.ReadAllText(), Is.EqualTo(content));
        }

        [Test]
        public void AddFile_CreateFileWithBytesContent()
        {
            var content = new byte[] { 0x12, 0x10, (byte)1 };
            var file = _provider.CreateFile("test.txt", content);
            using (var stream = file.OpenRead())
            {
                // Don't use .Length to support non-searchable streams
                var bytes = new List<byte>();
                var next = 0;
                while ((next = stream.ReadByte()) > -1)
                {
                    bytes.Add((byte)next);
                }

                Assert.That(bytes, Is.EquivalentTo(content));
            }
        }

        [Test]
        public void CombineVirtualPath_ReturnValue()
        {
            var path = _provider.CombineVirtualPath("folder1", "folder2");
            Assert.That(path, Is.Not.Null);
        }

        [Test]
        public void CombineVirtualPath_ReturnCorrectPath()
        {
            var path1 = "folder1";
            var path2 = "folder2";

            var dir = _provider.CreateDirectory(path1).CreateDirectory(path2);
            var path = _provider.CombineVirtualPath(path1, path2);
            Assert.That(_provider.GetDirectory(path), Is.EqualTo(dir));
        }

        [Test]
        public void DirectoryExists_ReturnCorrectValue()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            Assert.That(_provider.DirectoryExists("/Folder1"), Is.True);
            Assert.That(_provider.DirectoryExists("/xFolder1"), Is.False);
        }
        
        [Test]
        public void FileExists_ReturnCorrectValue()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            Assert.That(_provider.FileExists("/File1.ext"), Is.True);
            Assert.That(_provider.FileExists("/xFiles"), Is.False);
        }
        
        [Test]
        public void ReturnAllMatchingFiles_ReturnAllFilesInProvider()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            _provider.RootDirectory.CreateFile("File2.ext", "");
            _provider.RootDirectory.CreateFile("File3.ext", "");
            _provider.RootDirectory.CreateDirectory("Folder1");
            _provider.RootDirectory.CreateFile("Folder1/File1.ext", "");

            var files = _provider.GetAllMatchingFiles("*.ext", int.MaxValue);
            Assert.That(files.Select(d => d.VirtualPath), Is.EquivalentTo(new[]
            {
                "/File1.ext",
                "/File2.ext",
                "/File3.ext",
                "/Folder1/File1.ext",
            }));
        }

        [Test]
        [Timeout(100000)]
        public void GetDirectory_ReturnDirectory()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            var dir = _provider.GetDirectory("Folder1");
            Assert.That(dir, Is.Not.Null);
            Assert.That(dir.IsDirectory, Is.True);
        }

        [Test]
        public void GetFile_ReturnFile()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            var file = _provider.GetFile("/File1.ext");
            Assert.That(file, Is.Not.Null);
            Assert.That(file.IsDirectory, Is.False);
        }

        [Test]
        public void GetFileHash_ReturnHash()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            var fileHash = _provider.GetFileHash("/File1.ext");
            Assert.That(fileHash, Is.Not.Null);
        }

        [Test]
        public void RootDirectory_IsNotNull()
        {
            Assert.That(_provider.RootDirectory, Is.Not.Null);
        }

        [Test]
        public void Directory_AddDirectory_CreateDirectory()
        {
            var dir = _provider.RootDirectory.CreateDirectory("Folder1");
            Assert.That(dir, Is.Not.Null);
        }

        [Test]
        public void Directory_AddDirectory_AddDirectoryToProvider()
        {
            var dir = _provider.RootDirectory.CreateDirectory("Folder1");
            Assert.That(_provider.GetDirectory("Folder1"), Is.EqualTo(dir));
        }

        [Test]
        public void Directory_AddDirectory_AddDirectoryToParent()
        {
            var dir = _provider.RootDirectory.CreateDirectory("Folder1");
            Assert.That(_provider.RootDirectory.GetDirectory("Folder1"), Is.EqualTo(dir));
        }

        [Test]
        public void Directory_AddDirectory_AddDirectoryToParent_WhenParentAlreadyFetched()
        {
            var dir = _provider.RootDirectory;
            var subDirs = dir.Directories.ToArray();
            var newDir = dir.CreateDirectory("Folder1");
            Assert.That(dir.GetDirectory("Folder1"), Is.EqualTo(newDir));
        }

        [Test]
        public void Directory_AddDirectory_AddDirectoryToParent_WhenCalledWithPath()
        {
            var dir = _provider.RootDirectory.CreateDirectory("/Folder1");
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder1"), Is.EqualTo(dir));
            Assert.That(_provider.RootDirectory.GetDirectory("Folder1"), Is.EqualTo(dir));
        }

        [Test]
        public void Directory_AddDirectory_DoNotAddDirectoryToRoot_WhenCalledWithPath()
        {
            var dir = _provider.RootDirectory.CreateDirectory("/Folder1");
            var subDir = dir.CreateDirectory("/Folder2");

            Assert.That(dir.GetDirectory("/Folder2"), Is.EqualTo(subDir));
            Assert.That(_provider.GetDirectory("/Folder1/Folder2"), Is.EqualTo(subDir));
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder2"), Is.Null);
        }

        [Test]
        public void Directory_AddDirectory_CreateIntermediatesDirectories_WhenCalledWithDeepPath()
        {
            var dir = _provider.RootDirectory.CreateDirectory("/Folder1/SubFolder1");
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder1"), Is.Not.Null);
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder1/SubFolder1"), Is.EqualTo(dir));
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder1").GetDirectory("SubFolder1"), Is.EqualTo(dir));
        }
        
        [Test]
        public void Directory_AddDirectory_AddDirectoryToParent_WhenCalledWithDeepPath()
        {
            var dir = _provider.RootDirectory.CreateDirectory("/Folder1/SubFolder1");
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder1/SubFolder1"), Is.EqualTo(dir));
            Assert.That(_provider.RootDirectory.GetDirectory("/Folder1").GetDirectory("SubFolder1"), Is.EqualTo(dir));
        }

        [Test]
        public void Directory_AddDirectory_DontThrowException_WhenAlreadyExists()
        {
            var dir1 = _provider.RootDirectory.CreateDirectory("/Folder1");
            var dir2 = _provider.RootDirectory.CreateDirectory("/Folder1");

            Assert.That(dir1, Is.EqualTo(dir2));
        }

        [Test]
        public void Directory_AddFile_ThrowArgumentException_WhenFileExists()
        {
            _provider.CreateFile("File1.ext", "");
            Assert.Throws<ArgumentException>(() => _provider.CreateFile("File1.ext", ""));
        }

        [Test]
        public void Directory_AddFile_CreateFile()
        {
            var name = "test.txt";
            var file = _provider.RootDirectory.CreateFile(name, "");

            Assert.That(file, Is.Not.Null);
            Assert.That(file.IsDirectory, Is.False);
            Assert.That(file.Name, Is.EqualTo(name));
        }

        [Test]
        public void Directory_AddFile_AddFileToProvider()
        {
            var name = "test.txt";
            var file = _provider.RootDirectory.CreateFile(name, "");

            Assert.That(_provider.GetFile(name), Is.EqualTo(file));
        }

        [Test]
        public void Directory_AddFile_AddFileToDirectory()
        {
            var name = "test.txt";
            var file = _provider.RootDirectory.CreateFile(name, "");

            Assert.That(_provider.RootDirectory.GetFile(name), Is.EqualTo(file));
        }

        [Test]
        public void Directory_AddFile_CreateFileWithStringContent()
        {
            var content = "blabla";
            var file = _provider.RootDirectory.CreateFile("test.txt", content);
            Assert.That(file.ReadAllText(), Is.EqualTo(content));
        }

        [Test]
        public void Directory_AddFile_CreateFileWithBytesContent()
        {
            var content = new byte[] { 0x12, 0x10, (byte)1 };
            var file = _provider.RootDirectory.CreateFile("test.txt", content);
            using (var stream = file.OpenRead())
            {
                // Don't use .Length to support non-searchable streams
                var bytes = new List<byte>();
                var next = 0;
                while ((next = stream.ReadByte()) > -1)
                {
                    bytes.Add((byte)next);
                }

                Assert.That(bytes, Is.EquivalentTo(content));
            }
        }

        [Test]
        public void Directory_AddFile_CreateFileWithStream()
        {
            var content = new byte[] { 0x12, 0x10, (byte)1 };
            using (var stream = _provider.RootDirectory.CreateFile("test.txt"))
            {
                stream.Write(content, 0, content.Length);
            }
            var file = _provider.GetFile("test.txt");
            using (var stream = file.OpenRead())
            {
                // Don't use .Length to support non-searchable streams
                var bytes = new List<byte>();
                var next = 0;
                while ((next = stream.ReadByte()) > -1)
                {
                    bytes.Add((byte)next);
                }

                Assert.That(bytes, Is.EquivalentTo(content));
            }
        }

        [Test]
        public void Directory_Delete_DeleteSubDirectory()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            _provider.RootDirectory.Delete("Folder1");
            Assert.That(_provider.RootDirectory.GetDirectory("Folder1"), Is.Null);
            Assert.That(_provider.DirectoryExists("Folder1"), Is.False);
        }

        [Test]
        public void Directory_Delete_DeleteItSelf()
        {
            var dir = _provider.RootDirectory.CreateDirectory("Folder1");
            dir.Delete();
            Assert.That(_provider.RootDirectory.GetDirectory("Folder1"), Is.Null);
            Assert.That(_provider.DirectoryExists("Folder1"), Is.False);
        }

        [Test]
        public void Directory_Delete_DeleteSubDirectory_WhenCalledWithDeepPath()
        {
            _provider.RootDirectory.CreateDirectory("Folder1/Folder2/Folder3");
            _provider.RootDirectory.Delete("Folder1/Folder2/Folder3");
            var deletedDir = _provider.RootDirectory.GetDirectory("Folder1/Folder2/Folder3");
            Assert.That(deletedDir, Is.Null);
            Assert.That(_provider.RootDirectory.GetDirectory("Folder1/Folder2"), Is.Not.Null);
        }

        [Test]
        public void Directory_Delete_DoNotThrow_WhenDeleteNotExistingNode()
        {
            _provider.RootDirectory.Delete("Folder2");
            _provider.RootDirectory.Delete("File1.ext");
        }

        [Test]
        public void Directory_Name_IsCorrect()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            var dir = _provider.RootDirectory.GetDirectory("Folder1");
            Assert.That(dir.Name, Is.EqualTo("Folder1"));
        }

        [Test]
        public void Directory_LastModified_DoNotThrow()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            var dir = _provider.RootDirectory.GetDirectory("Folder1");
            var date = dir.LastModified;
        }

        [Test]
        public void Directory_ToString_IsNotNull()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            var dir = _provider.RootDirectory.GetDirectory("Folder1");

            Assert.That(dir.ToString(), Is.Not.Null);
        }

        [Test]
        public void Directory_RealPath_IsNotNull()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            var dir = _provider.RootDirectory.GetDirectory("Folder1");

            Assert.That(dir.RealPath, Is.Not.Null);
        }

        [Test]
        public void File_RealPath_IsNotNull()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            var file = _provider.RootDirectory.GetFile("File1.ext");
            Assert.That(file.RealPath, Is.Not.Null);
        }

        [Test]
        public void File_Extension_IsCorrect()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            var file = _provider.RootDirectory.GetFile("File1.ext");
            Assert.That(file.Extension, Is.EqualTo(".ext"));
        }

        [Test]
        public void File_LastModified()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            var file = _provider.RootDirectory.GetFile("File1.ext");
            var date = file.LastModified;
        }

        [Test]
        public void Node_Directory_IsNotNull()
        {
            _provider.RootDirectory.CreateFile("File1.ext", "");
            var file = _provider.RootDirectory.GetFile("File1.ext");
            Assert.That(file.Directory, Is.Not.Null);

            Assert.That(_provider.RootDirectory.Directories, Is.Not.Null);
        }

        [Test]
        public void Directory_Directories_ReturnCorrectValue()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            _provider.RootDirectory.CreateDirectory("Folder2");

            var dirs = _provider.RootDirectory.Directories;

            Assert.That(dirs.Select(d => d.Name), Is.EquivalentTo(new[]
            {
                "Folder1",
                "Folder2"
            }));
        }

        [Test]
        public void Directory_Files_ReturnCorrectValue()
        {
            _provider.RootDirectory.CreateDirectory("Folder1");
            _provider.RootDirectory.CreateFile("File1.ext", "");
            _provider.RootDirectory.CreateFile("File2.ext", "");
            _provider.RootDirectory.CreateFile("File3.ext", "");

            var files = _provider.RootDirectory.Files;

            Assert.That(files.Select(d => d.Name), Is.EquivalentTo(new[]
            {
                "File1.ext",
                "File2.ext",
                "File3.ext",
            }));
        }

        [Test]
        public void File_OpenWrite()
        {
            var initial = new byte[] { 0x00, 0x01, 0x02 };
            var content = new byte[] { 0x03, 0x04 };
            var expected = new byte[] { 0x03, 0x04, 0x02 };
            var file = _provider.CreateFile("test.txt", initial);
            using (var stream = file.OpenWrite())
            {
                stream.Write(content, 0, content.Length);
            }
            Assert.That(file.ReadAllBytes(), Is.EquivalentTo(expected));
        }

        [Test]
        public void File_OpenWrite_Append()
        {
            var initial = new byte[] { 0x00, 0x01, 0x02 };
            var content = new byte[] { 0x03, 0x04 };
            var expected = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04 };
            var file = _provider.CreateFile("test.txt", initial);
            using (var stream = file.OpenWrite(WriteMode.Append))
            {
                stream.Write(content, 0, content.Length);
            }
            Assert.That(file.ReadAllBytes(), Is.EquivalentTo(expected));
        }

        [Test]
        public void File_OpenWrite_Truncate()
        {
            var initial = new byte[] { 0x00, 0x01, 0x02 };
            var content = new byte[] { 0x03, 0x04 };
            var file = _provider.CreateFile("test.txt", initial);
            using (var stream = file.OpenWrite(WriteMode.Truncate))
            {
                stream.Write(content, 0, content.Length);
            }
            Assert.That(file.ReadAllBytes(), Is.EquivalentTo(content));
        }

        [Test]
        public void File_Move_UsingIVirtualDirectory()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderMove");

            var newFile = file.MoveTo(dir);

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Null);
        }

        [Test]
        public void File_Move_UsingIVirtualDirectoryAndName()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderMove");

            var newFile = file.MoveTo(dir, "newfile.txt");

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Name, Is.EqualTo("newfile.txt"));
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Null);
        }

        [Test]
        public void File_Move_UsingDestinationVirtualPath()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderMove");

            var newFile = file.MoveTo("FolderMove");

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Null);
        }

        [Test]
        public void File_Move_UsingDestinationVirtualPathAndName()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderMove");

            var newFile = file.MoveTo("FolderMove", "newfile.txt");

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Name, Is.EqualTo("newfile.txt"));
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Null);
        }

        [Test]
        public void File_Move_CrossProvider()
        {
            using (var otherProvider = new InMemoryVirtualPathProvider())
            {
                var file = _provider.CreateFile("test.txt", "test data");
                var dir = otherProvider.CreateDirectory("FolderMove");

                var newFile = file.MoveTo(dir);

                Assert.That(newFile, Is.Not.Null);
                Assert.That(newFile.Directory, Is.EqualTo(dir));
                Assert.That(dir.GetFile("test.txt"), Is.EqualTo(newFile));
                Assert.That(_provider.GetFile("test.txt"), Is.Null);
            }
        }

        [Test]
        public void File_Copy_UsingIVirtualDirectory()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderCopy");

            var newFile = file.CopyTo(dir);

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Not.Null);
        }

        [Test]
        public void File_Copy_UsingIVirtualDirectoryAndName()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderCopy");

            var newFile = file.CopyTo(dir, "newfile.txt");

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Name, Is.EqualTo("newfile.txt"));
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Not.Null);
        }

        [Test]
        public void File_Copy_UsingDestinationVirtualPath()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderCopy");

            var newFile = file.CopyTo("FolderCopy");

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Not.Null);
        }

        [Test]
        public void File_Copy_UsingDestinationVirtualPathAndName()
        {
            var file = _provider.CreateFile("test.txt", "test data");
            var dir = _provider.CreateDirectory("FolderCopy");

            var newFile = file.CopyTo("FolderCopy", "newfile.txt");

            Assert.That(newFile, Is.Not.Null);
            Assert.That(newFile.Name, Is.EqualTo("newfile.txt"));
            Assert.That(newFile.Directory, Is.EqualTo(dir));
            Assert.That(_provider.GetFile("test.txt"), Is.Not.Null);
        }

        [Test]
        public void File_Copy_CrossProvider()
        {
            using (var otherProvider = new InMemoryVirtualPathProvider())
            {
                var file = _provider.CreateFile("test.txt", "test data");
                var dir = otherProvider.CreateDirectory("FolderMove");

                var newFile = file.CopyTo(dir);

                Assert.That(newFile, Is.Not.Null);
                Assert.That(newFile.Directory, Is.EqualTo(dir));
                Assert.That(dir.GetFile("test.txt"), Is.EqualTo(newFile));
                Assert.That(_provider.GetFile("test.txt"), Is.Not.Null);
            }
        }
    }
}
