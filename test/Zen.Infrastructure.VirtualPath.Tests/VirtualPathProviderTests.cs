﻿using NUnit.Framework;
using Zen.Infrastructure.VirtualPath.FileSystem;
using Zen.Infrastructure.VirtualPath.InMemory;

namespace Zen.Infrastructure.VirtualPath.Tests
{
    public class VirtualPathProviderTests
    {
        [Test]
        public void Open_ReturnInMemoryProvider()
        {
            var provider = VirtualPathProvider.Open("Memory");
            Assert.That(provider, Is.InstanceOf<InMemoryVirtualPathProvider>());
        }

        [Test]
        public void Open_ReturnFileSystemProvider_WithPathParameter()
        {
            var provider = VirtualPathProvider.Open("FS", "C:\\temp");
            Assert.That(provider, Is.InstanceOf<FileSystemVirtualPathProvider>());
        }
        
        [Test]
        public void Open_ReturnFileSystemProvider_WithConfigurationString()
        {
            var provider = VirtualPathProvider.Open("FS", "rootPath=C:\\temp");
            Assert.That(provider, Is.InstanceOf<FileSystemVirtualPathProvider>());
        }

        [Test]
        public void Open_ReturnProvider()
        {
            Assert.That(VirtualPathProvider.Open("Memory"), Is.InstanceOf<IVirtualPathProvider>());
            Assert.That(VirtualPathProvider.Open("FS", "C:\\temp"), Is.InstanceOf<IVirtualPathProvider>());
        }
    }
}
