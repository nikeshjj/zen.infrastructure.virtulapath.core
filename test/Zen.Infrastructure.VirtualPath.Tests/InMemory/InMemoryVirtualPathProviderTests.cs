﻿using Zen.Infrastructure.VirtualPath.InMemory;

namespace Zen.Infrastructure.VirtualPath.Tests.InMemory
{
    public class InMemoryVirtualPathProviderTests : AbstractVirtualPathProviderFixture
    {
        public InMemoryVirtualPathProviderTests()
            : base(() => new InMemoryVirtualPathProvider())
        { }
    }
}
