﻿using Zen.Infrastructure.VirtualPath.Tests;

namespace Zen.Infrastructure.VirtualPath.SshNet.Tests
{
    public class AbstractVirtualPathProviderTests : AbstractVirtualPathProviderFixture
    {
        public AbstractVirtualPathProviderTests()
            : base(() => new SftpVirtualPathProvider("localhost", 22, "demo", "demo"))
        { }
    }
}
