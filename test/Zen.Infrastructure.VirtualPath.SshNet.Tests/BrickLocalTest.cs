using System;
using NUnit.Framework;
using Zen.Infrastructure.VirtualPath.SshNet.BrickFtp.Client;

namespace Zen.Infrastructure.VirtualPath.SshNet.Tests
{
    public class BrickLocalTest
    {
        [Test]
        public void GetUsers()
        {
          //  var provider = VirtualPathProvider.Open("BrickFtp", "host=https://znergysuite.brickftp.com/api/rest/v1;apiKey=d5cbcf464ed202fbde0f4a352f89466a186007d8");
            var client = new BrickFtpApiClient("https://znergysuite.brickftp.com/api/rest/v1","d5cbcf464ed202fbde0f4a352f89466a186007d8");
            var result = client.GetUsers();
            foreach (var item in result)
            {
                Console.WriteLine("username: {0}, name: {1}", item.username, item.name);
            }
        }

        [TestCase("/")]
        [TestCase("/AMPZ")]
        [TestCase("/AMPZ/FT1")]
        public void GetDirectories(string path)
        {
            var provider = VirtualPathProvider.Open("BrickFtp", "apiKey=d5cbcf464ed202fbde0f4a352f89466a186007d8");
            var directory = provider.GetDirectory(path);
            foreach (var item in directory.Directories)
            {
                Console.WriteLine("path: {0}, type: dir", item.Name);
            }
            Console.WriteLine();

            foreach (var item in directory.Files)
            {
                Console.WriteLine("path: {0}, type: file", item.Name);
            }
        }

        [Test]
        public void GetFile()
        {
            var provider = VirtualPathProvider.Open("BrickFtp", "apiKey=d5cbcf464ed202fbde0f4a352f89466a186007d8");
            var result = provider.GetFile("/AMPZ/desktopcentral_license.xml");
            Console.WriteLine("path: {0}", result.Name);
        }

        [Test]
        public void DownloadFile()
        {
            var provider = VirtualPathProvider.Open("BrickFtp", "apiKey=d5cbcf464ed202fbde0f4a352f89466a186007d8");
            var result = provider.GetFile("/AMPZ/desktopcentral_license.xml");
            using (var reader = result.OpenText())
            {
                Console.WriteLine(reader.ReadToEnd());
            }
        }

        [Test]
        public void Rename()
        {
            var provider = VirtualPathProvider.Open("BrickFtp", "apiKey=0be75964afd054928fa862b7320329700f8c12ba");
            var result = provider.GetFile("/AMPZ/desktopcentral_license2.xml");
            result.RenameTo("desktopcentral_license.xml");
        }
    }
}