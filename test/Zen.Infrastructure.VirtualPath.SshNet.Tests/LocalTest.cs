using System.Linq;
using NUnit.Framework;
using Zen.Infrastructure.VirtualPath.FileSystem;

namespace Zen.Infrastructure.VirtualPath.SshNet.Tests
{
    public class LocalTest
    {
        [Test]
        public void Scenario1()
        {
            var fsProvider = new FileSystemVirtualPathProvider("C:\\Temp\\Spikes\\Destination");
            var fsDirectory = fsProvider.GetDirectory("/NAP/INT/Out/Encrypted");

            var ftpProvider = new SftpVirtualPathProvider("drive.znergysuite.com", 22, "zn-nap-esg-int",
                "X1786YPJJ119zu6R8ZRS");
            var ftpDirectory = ftpProvider.GetDirectory("/NAP/INT/ESG/Outbound");

            var files = fsDirectory.Files.ToArray();

            foreach (var file in files)
            {
                var newFile = file.MoveTo(ftpDirectory, file.Name + ".processing");
                newFile.RenameTo(file.Name);
            }
        }
    }
}