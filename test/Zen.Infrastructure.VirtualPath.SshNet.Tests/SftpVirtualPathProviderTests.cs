﻿using NUnit.Framework;

namespace Zen.Infrastructure.VirtualPath.SshNet.Tests
{
    public class SftpVirtualPathProviderTests
    {
        [Test]
        public void ShouldBeDisposable()
        {
            var provider = new SftpVirtualPathProvider("localhost", "demo", "demo");
            provider.Dispose();
        }

        [Test]
        public void ShouldHaveConstructorWithPort()
        {
            var provider = new SftpVirtualPathProvider("localhost", 22, "demo", "demo");
        }
    }
}
